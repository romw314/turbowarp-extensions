# Contributing

> ***Warning:***
>
> These contributing guidelines are for an older version of this project ([23f6395cc1dbf0abd051f7fe140705290ad51963](https://gitlab.com/romw314/turbowarp-extensions/-/tree/23f6395cc1dbf0abd051f7fe140705290ad51963)).
>

## Creating extensions

If you want to add your own extension, you can. Place your own extensions under the `public/extensions/community` directory and submit a merge request. The extension id shouldn't begin with `romw314` or `romwthreeonefour` or `romwthreehundredfourteen`. If you have time, please create an example that uses as much as you can blocks of the extension and describes what is the program doing, what are the blocks used for and why in comments. Place the examples under the `public/examples/community` directory.

### Using variables

**Never** define variables using `var`. Use `let`, or `const` if you don't need `let`. Prefer using properties of your extension class over using `const` or `let` outside the class.

Example:

```javascript
// Wrong code:
var x = 1;
class M {
	myFunc() {
		x++;
		if (x > 6)
			return 'Hello!';
		else
			return 'Oh, call me again, please.';
	}
}

// Good code
class M {
	x = 1;
	myFunc() {
		this.x++;
		if (x > 6)
			return 'Hello!';
		else
			return 'Oh, call me again, please.';
	}
}

// Wrong code
class M {
	myFunc() {
		var x = 1;
		while (x < 9) {
			x += Math.min(3, parseInt(window.prompt('Enter the number to increase X by:')));
		}
		window.alert('X is 9! Awesome!');
	}
}

// Good code
class M {
	myFunc() {
		let x = 1;
		while (x < 9) {
			x += Math.min(3, parseInt(window.prompt('Enter the number to increase X by:')));
		}
		window.alert('X is 9! Awesome!');
	}
}

// Wrong code
let myObj = {
	a: 1,
	b: 2
};
if (someCondition)
	myObj.a = 2;
if (anotherCondition)
	myObj.b = 1;
// myObj = {} will pass

// Good code
const myObj = {
	a: 1,
	b: 2
};
if (someCondition)
	myObj.a = 2;
if (anotherCondition)
	myObj.b = 1;
// myObj = {} will fail
```

### `c` variable

Extensions should (but don't need to) have a `c` variable declaration at the beginning, which is the extension author's nickname or username, and it should be used in the `getInfo` function:

```javascript
const c = 'myusername';
class MyExtension {
	getInfo() {
		return {
			id: `${c}myextension`,
			// ...
		};
	}
	// ...
}
// ...
```

### Naming

The extension file should have a short name separated by hyphens (`-`) and have the `.js` extension, for example, `date-utils.js`.

The example file should have the exactly same name as the full name of the extension.

### Using libraries

**Never** copy-paste libraries into extensions, because this is very ugly. It seems that [Rollup](https://rollupjs.org/) is not working with Turbowarp extensions, so we have no way to use libraries in extensions.

If you know how to configure [Rollup](https://rollupjs.org/) (preferred) or [Parcel](https://parceljs.org) to bundle Turbowarp extensions, please do it and submit a merge request. Thanks a lot.

## Requesting extensions

If you want to request an extension, feel free to open an issue and I will look at it.
