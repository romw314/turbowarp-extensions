# TurboWarp extensions

My personal extension set for [TurboWarp](https://turbowarp.org/editor).

[![CircleCI](https://dl.circleci.com/status-badge/img/circleci/S66FZnTFuLNB2ZgMMBVtKW/Wm4HTnDgX5ekz8HMZ48DoT/tree/master.svg?style=svg&circle-token=74e6b3d8e0435598e8677ad6124ab918d8706419)](https://dl.circleci.com/status-badge/redirect/circleci/S66FZnTFuLNB2ZgMMBVtKW/Wm4HTnDgX5ekz8HMZ48DoT/tree/main)

Load the URLs below into TurboWarp using **Custom Extension**.

## Extensions

See [this page](https://gitlab.com/romw314/turbowarp-extensions/-/wikis/home) for the full list of extensions.

## Contributing

Follow [the contributing guidelines](https://gitlab.com/romw314/turbowarp-extensions/-/blob/master/CONTRIBUTING.md) if you want to contribute to this repository.
