document.addEventListener('DOMContentLoaded', () => {
	const searchbox = document.getElementById('searchbox');
	const includeUnsafe = document.getElementById('includeunsafe');
	const data = document.getElementById('db');

	function multiAttrBool(elem, ...attrs) {
		for (const attr of attrs) {
			console.log(elem, 'has', attr, 'set to', elem.getAttribute(attr));
			if (elem.getAttribute(attr) !== 'true')
				return false;
		}
		return true;
	}

	function includeUnsafeIf(condition) {
		for (const elem of data.childNodes) {
			if (!elem || !elem.classList || !elem.classList.contains('ext'))
				continue;
			elem.setAttribute('data-safe-show', condition ? true : elem.getAttribute('data-safe'));
			elem.style.display = multiAttrBool(elem, 'data-search-show', 'data-safe-show') ? '' : 'none';
		}
	}

	function search(str) {
		for (const elem of data.childNodes) {
			if (!elem || !elem.classList || !elem.classList.contains('ext'))
				continue;
			const resultArr = [...elem.childNodes].flatMap(node => (node && node.classList && node.classList.contains('extcell_Name')) ? [node.innerText.toLowerCase().includes(str)] : []);
			elem.setAttribute('data-search-show', resultArr.includes(true));
			elem.style.display = multiAttrBool(elem, 'data-search-show', 'data-safe-show') ? '' : 'none';
		}
	}

	searchbox.addEventListener('input', () => search(searchbox.value));
	includeUnsafe.addEventListener('input', () => includeUnsafeIf(includeUnsafe.checked));
	search('');
	includeUnsafeIf(false);
});
