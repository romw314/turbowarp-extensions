const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');
const querystring = require('querystring');
const { escape } = require('html-escaper');
const moment = require('moment');

const docPath = path.resolve('src/extensions.yaml');
const templatePath = path.resolve('src/extdb.html.in');
const outPath = path.resolve('out/extdb.html');

const doc = (() => {
	try {
		console.log(`Reading ${docPath}...`);
		return yaml.load(fs.readFileSync(docPath, 'utf8'));
	} catch (e) {
		console.error(e);
		process.exit(1);
	}
})();

function getlink(text, link, download) {
	if (!link)
		return getlink(text, text);
	return `<a href="${escape(link)}"${download ? ' download' : ''}>${escape(text)}</a>`;
}

console.log(`Reading ${templatePath}...`);
const template = fs.readFileSync(templatePath, 'utf8');

const site_url = process.env.NODE_ENV === 'production' ? doc.prod_url : (process.argv.includes('--dev') ? doc.dev_url : doc.prod_url);

// Objects don't store the order of the options so this array stores it.
const tableHeaders = [
	'Name',
	'Safe',
	'Documentation',
	'Turbowarp URL',
	'Example',
	'Open',
	'Download'
];

let items = [];
//`<tr id="extension_${escape(ext.id)}"><td>${escape(ext.name)}</td><td>${(ext.path && ext.url_visible) ? getlink(fullPath) : '-'}</td><td>${ext.example ? getlink('Example', doc.turbowarp_url + querystring.stringify(exampleQuery)) : '-'}</td><td>${ext.path ? getlink('Open', doc.turbowarp_url + querystring.stringify(openQuery)) : '-'}</td><td></td></tr>`;

for (const ext of doc.extensions) {
	console.log(`Building ${ext.name}...`);
	const fullPath = site_url + doc.extension_path + ext.path;
	const openQuery = (ext.sandbox === false) ? { unsandboxed: fullPath } : { extension: fullPath };
	const exampleQuery = { project_url: site_url + doc.example_path + ext.example };
	items.push({
		_htmlId: escape(ext.id),
		_safe: ext.safe,
		'Name':          escape(ext.name),
		'Safe':          (ext.safe === false) ? '<strong>No</strong>' : ((ext.safe === true) ? 'Yes' : '<em>Unknown</em>'),
		'Documentation': ext.doc ? getlink('Documentation', doc.repo_url + doc.doc_path + ext.doc) : '-',
		'Turbowarp URL': (ext.path && ext.url_visible) ? getlink(fullPath) : '-',
		'Example':       ext.example ? getlink('Example', doc.turbowarp_url + querystring.stringify(exampleQuery)) : '-',
		'Open':          ext.path ? getlink('Open', doc.turbowarp_url + querystring.stringify(openQuery)) : '-',
		'Download':      ext.path ? getlink('Download', fullPath, true) : '-'
	});
}

console.log(`Applying template...`);
const result = template
	.replace('{% db_header %}', tableHeaders.map(header => `<th>${header}</th>`).join(''))
	.replace('{% db_content %}', items.map(item => {
		let result = `<tr id="ext_${item._htmlId}" class="ext" data-safe="${Boolean(item._safe)}">`;
		for (const cell of tableHeaders) {
			result += `<td id="extcell_${item._htmlId}_${cell}" class="extcell extcell_${item._htmlId} extcell_${cell}">${item[cell]}</td>`;
		}
		return result;
	}).join(''))
	.replace('{% gen_date %}', moment().toDate().toUTCString());

console.log(`Writing ${outPath}...`);
fs.writeFileSync(outPath, result);
