const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');
const { globSync } = require('glob');
const LineByLineReader = require('line-by-line');

const doc = yaml.load(fs.readFileSync(path.resolve('src/extensions.yaml'), 'utf8'));

describe('file id = extensions yaml id', () => {
	for (const filename of globSync('src/extensions/**/*.js', { ignore: '**/*.test.js' })) {
		test(filename, async () => {
			const lr = new LineByLineReader(path.resolve(filename));

			lr.on('error', err => expect().fail(err.message));

			let reading = false;
			let metadata = '';

			lr.on('line', line => {
				if (reading) {
					if (line.trim() === '*/')
						lr.close();
					else
						metadata += line + '\n';
				}
				if (line.trim() === '/* ext_metadata')
					reading = true;
			});

			await new Promise(resolve => lr.on('end', resolve));

			expect(doc.extensions).toPartiallyContain(yaml.load(metadata));
		});
	}
});
