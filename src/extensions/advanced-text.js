/* ext_metadata
id: romwthreeonefouradvancedtext
path: advanced-text.js
*/

// @ts-expect-error
const c = 'romwthreeonefour';

class AdvancedTextExtension {
	getInfo() {
		return {
			id: `${c}advancedtext`,
			name: Scratch.translate('Advanced Text'),
			blocks: [
				{
					opcode: 'padLeft',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('pad [STRING] to [LENGTH] characters from left with [PAD_STRING]'),
					arguments: {
						STRING: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'HELLO'
						},
						LENGTH: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '10'
						},
						PAD_STRING: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: '.'
						}
					}
				},
				{
					opcode: 'padRight',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('pad [STRING] to [LENGTH] characters from right with [PAD_STRING]'),
					arguments: {
						STRING: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'HELLO'
						},
						LENGTH: {
							type: Scratch.ArgumentType.NUMBER,
							defaultValue: '10'
						},
						PAD_STRING: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: '-'
						}
					}
				}
			]
		};
	}
	padLeft({ STRING, LENGTH, PAD_STRING }) {
		return STRING.toString().padStart(Scratch.Cast.toNumber(LENGTH), PAD_STRING);
	}
	padRight({ STRING, LENGTH, PAD_STRING }) {
		return STRING.toString().padEnd(Scratch.Cast.toNumber(LENGTH), PAD_STRING);
	}
}
Scratch.extensions.register(new AdvancedTextExtension());
