/* ext_metadata
id: romwthreeonefourconditionalscopes
*/

// @ts-expect-error
const c = 'romwthreeonefour';

class ConditionalScopesExtension {
	scopes = {};

	getInfo() {
		return {
			id: `${c}conditionalscopes`,
			name: Scratch.translate('Conditional scopes'),
			color1: '#a500a5',
			color2: '#800080',
			color3: '#401240',
			blocks: [
				{
					opcode: 'changeScope',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('[MODE] scope [SCOPE]'),
					arguments: {
						MODE: {
							type: Scratch.ArgumentType.STRING,
							menu: 'MODE_MENU',
							defaultValue: 'activate'
						},
						SCOPE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'hello'
						}
					}
				},
				{
					opcode: 'setScope',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('set [SCOPE] activation to [MODE]'),
					arguments: {
						SCOPE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'hello'
						},
						MODE: {
							type: Scratch.ArgumentType.BOOLEAN
						}
					}
				},
				{
					opcode: 'scope',
					blockType: Scratch.BlockType.CONDITIONAL,
					text: Scratch.translate('scope [SCOPE]'),
					arguments: {
						SCOPE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'hello'
						}
					}
				},
				{
					opcode: 'invertedScope',
					blockType: Scratch.BlockType.CONDITIONAL,
					text: Scratch.translate('inverted scope [SCOPE]'),
					arguments: {
						SCOPE: {
							type: Scratch.ArgumentType.STRING,
							defaultValue: 'hello'
						}
					}
				},
				{
					opcode: 'deleteAll',
					blockType: Scratch.BlockType.COMMAND,
					text: Scratch.translate('delete all scopes'),
					arguments: {}
				},
				{
					opcode: 'getScopesJSON',
					blockType: Scratch.BlockType.REPORTER,
					text: Scratch.translate('all scope states as JSON'),
					disableMonitor: true,
					arguments: {}
				}
			],
			menus: {
				MODE_MENU: {
					acceptReporters: true,
					items: ['activate', 'deactivate']
				}
			}
		};
	}
	changeScope({ MODE, SCOPE }) {
		this.setScope({ SCOPE, MODE: MODE === 'activate' });
	}
	setScope({ SCOPE, MODE }) {
		this.scopes[SCOPE] = MODE;
	}
	scope({ SCOPE }) {
		return this.scopes[SCOPE];
	}
	invertedScope(args) {
		return !this.scope(args);
	}
	deleteAll() {
		this.scopes = {};
	}
	getScopesJSON() {
		return JSON.stringify(this.scopes);
	}
}
Scratch.extensions.register(new ConditionalScopesExtension());
